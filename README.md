# Simple Storage Truffle Dapp

- Deploy the smart contract - truffle, ethereum, hdwallet-provider
- Build the Web3 frontend - Create React App, Web3.js

![dApp screenshot](screenshot.png)

&copy; 2023 @bcryptorider920

All Rights Reserved.
